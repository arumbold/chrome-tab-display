var myContext;
var imgLoader;
var store = [];

window.onload = function ()
{
	myCanvas = document.getElementById('myCanvas');
	myCanvas.width = screen.availWidth;
	myCanvas.height = screen.availHeight;
	myContext = myCanvas.getContext('2d');
	imgLoader = document.createElement("img");
	imgLoader.onload  = drawOnCanvas;
}

chrome.runtime.onMessageExternal.addListener(messageHandler);

function messageHandler(msg, sender, sendResponse){
    if (msg.tabImg)
    {
		imgLoader.src = msg.tabImg;
	}

	assist(msg);
}

function assist(item){
	console.log(item);
	store.push(item);
}

function drawOnCanvas() {
	// Draw image onto canvas
	myContext.drawImage(this,0,0);
}
